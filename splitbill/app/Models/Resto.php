<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Resto extends Model
{
    use HasFactory;

    protected $table = 'resto';
    protected $primaryKey = 'id';
    protected $fillable = [
        'nama_resto',
        'slug'
    ];

    public function menu()
    {
        return $this->hasMany(Menu::class);
    }

    public function tagihan()
    {
        return $this->hasMany(Tagihan::class);
    }

    public function orang()
    {
        return $this->hasMany(Orang::class);
    }
}
