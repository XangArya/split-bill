<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $table = 'menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'resto_id',
        'menu',
        'jumlah_pesanan',
        'harga'
    ];

    public function tagihan()
    {
        return $this->hasMany(Tagihan::class);
    }
}