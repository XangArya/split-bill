<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tagihan extends Model
{
    use HasFactory;

    protected $table = 'tagihan';
    protected $primaryKey = 'id';
    protected $fillable = [
        'resto_id',
        'orang_id',
        'menu_id',
    ];

    public function resto()
    {
        return $this->belongsTo('App\Models\Resto');
    }

    public function orang()
    {
        return $this->belongsTo('App\Models\Orang', 'orang_id');
    }
    
    public function menu()
    {
        return $this->belongsTo(Menu::class, 'menu_id', 'id');
    }
    
    
}
