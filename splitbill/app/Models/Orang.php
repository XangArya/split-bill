<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orang extends Model
{
    use HasFactory;

    protected $table = 'orang';
    protected $primaryKey = 'id';
    protected $fillable = [
        'resto_id',
        'nama'
    ];

    public function tagihan()
    {
        return $this->hasMany(Tagihan::class);
    }
}