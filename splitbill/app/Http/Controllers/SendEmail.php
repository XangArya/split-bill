<?php

namespace App\Http\Controllers;

use App\Models\Orang;
use App\Mail\SplitBills;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmail extends Controller
{
    public function index()
{
    $userId = 1;
    $user = Orang::find($userId);

    if ($user) {
        
        Mail::to($user->email)->send(new SplitBills($user));
        return response()->json(['message' => 'email terkirim bro']);
    } else {
        return response()->json(['error' => 'User not found'], 404);
    }
}

}