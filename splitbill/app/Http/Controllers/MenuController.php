<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use App\Models\Menu;
use App\Models\Resto;
use App\Models\Orang;
use App\Models\Tagihan;

class MenuController extends Controller
{
    public function dashboard()
    {
        // Mengambil semua data dari tabel Resto
        $resto = Resto::all();
    
        // Mengelompokkan data berdasarkan nama restoran
        $restogroup = $resto->groupBy('nama_resto');
    
        // Inisialisasi array untuk menyimpan hasil perhitungan persentase
        $restoPercentages = [];
    
        foreach ($restogroup as $namaResto => $dataResto) {
            // Menghitung jumlah data untuk setiap nama restoran
            $count = $dataResto->count();
    
            // Menghitung persentase untuk setiap nama restoran
            $percentage = ($count / $resto->count()) * 100;
    
            // Menyimpan hasil persentase ke dalam array dengan kunci nama restoran
            $restoPercentages[$namaResto] = $percentage;
        }
        
    
        return view('user.dashboard', compact('restoPercentages'));
    }
    

    public function resto()
    {
        $resto = Resto::all();
        return view('user.resto', compact('resto'));
    }

    public function resto_store(Request $request)
    {
        $request->validate([
            'nama_resto' => 'required',
        ]);

        $input = $request->all();

        // Generate a slug from the 'nama_resto' field
        $input['slug'] = Str::slug($request->input('nama_resto'));

        $resto = Resto::create($input);
        return Response::json($resto);
    }


    public function resto_update(Request $request, $id)
    {
        $request->validate([
            'nama_resto' => 'required',

        ]);

        $resto = resto::findOrFail($id);

        $resto->update([
            'nama_resto' => $request->input('nama_resto'),
            'slug' => $request->input('nama_resto')
        ]);

        return Response::json($resto);
    }

    public function resto_destroy(string $id)
    {
        $resto = Resto::find($id);

        if ($resto) {
            // Menghapus semua tagihan yang terkait dengan menu yang terkait dengan resto
            $resto->menu->each(function ($menu) {
                $menu->tagihan()->delete();
                $menu->delete();
            });

            // Menghapus semua orang yang terkait dengan resto
            $resto->orang()->delete();

            // Menghapus resto itu sendiri
            $resto->delete();
        }

        return redirect('/splitbill')->with('flash_message', 'Restoran deleted!');
    }

    public function orang_store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'resto_id' => 'required',
        ]);

        $input = $request->all();

        Orang::create($input);

        return redirect()->back()->with('success', 'Resto telah ditambahkan.');
    }
    public function coba()
    {
        $split = split::all();
        return view('coba', compact('split'));
    }

    public function orang_destroy(string $id)
    {
        $orang = Orang::find($id);

        if ($orang) {
            // Hapus semua tagihan yang terkait dengan Orang
            $orang->tagihan->each(function ($nama) {
                $nama->delete();
            });

            // Hapus Orang itu sendiri
            $orang->delete();

            return redirect()->back()->with('success', 'Orang Telah dihapus.');
        }

        return redirect()->back()->with('error', 'Orang tidak ditemukan.');
    }



    public function menu_store(Request $request)
    {
        $request->validate([
            'menu' => 'required',
            'harga' => 'required',
            'jumlah_pesanan' => 'required',
            'resto_id' => 'required',
        ]);

        $input = $request->all();
        $pesanan = (int) $input['pesanan'];
        $menuId = $request->input('menu_id'); // Assuming 'menu_id' is passed for editing

        if (empty($menuId)) {
            // Create a new menu item
            $menuItems = [];

            for ($i = 0; $i < $pesanan; $i++) {
                $menuItems[] = [
                    'menu' => $input['menu'],
                    'harga' => $input['harga'],
                    'jumlah_pesanan' => $input['jumlah_pesanan'],
                    'resto_id' => $input['resto_id'],
                    'created_at' => now(),
                    'updated_at' => now(),
                ];
            }

            Menu::insert($menuItems);
        } else {
            // Edit an existing menu item
            $menu = Menu::find($menuId);

            if ($menu) {
                $menu->update([
                    'menu' => $input['menu'],
                    'harga' => $input['harga'],
                    'jumlah_pesanan' => $input['jumlah_pesanan'],
                    'resto_id' => $input['resto_id'],
                ]);
            }
        }

        return redirect()->back()->with('success', 'Menu telah ditambahkan/selesai diubah.');
    }


    public function menu_destroy(string $id)
    {
        $menu = Menu::find($id);

        if ($menu) {
            // Hapus semua tagihan yang terkait dengan Orang
            $menu->tagihan->each(function ($nama) {
                $nama->delete();
            });

            // Hapus Orang itu sendiri
            $menu->delete();

            return redirect()->back()->with('success', 'Orang Telah dihapus.');
        }

        return redirect()->back()->with('error', 'Orang tidak ditemukan.');
    }

    public function tagihan(Request $request, $id = null)
    {
        $isEditMode = !is_null($id);
        $menu = Menu::where('resto_id', $id)->get();
        $orang = Orang::where('resto_id', $id)->get();

        $tagihan = Tagihan::join('menu', 'tagihan.menu_id', '=', 'menu.id')
            ->where('menu.resto_id', $id) // Filter based on resto_id
            ->get();

        $resto = Resto::where('id', $id)->firstOrFail();

        return view('user.tagihan', compact('resto', 'orang', 'menu', 'tagihan', 'isEditMode'));
    }

    public function tagihan_store(Request $request, $id)
    {
        $request->validate([
            'resto_id.*' => 'required',
            'orang_id.*' => 'required',
            'menu_id.*' => 'required',
        ]);

        // Get the arrays from the request
        $restoIds = $request->input('resto_id');
        $namaIds = $request->input('orang_id');
        $menuIds = $request->input('menu_id');

        // Get the existing records for this resto_id
        $existingRecords = Tagihan::where('resto_id', $id)->get();

        // Create an array to hold the data for bulk insertion and updating
        $dataToInsert = [];

        // Create an array to store menu_id values for existing records
        $existingMenuIds = [];

        foreach (range(0, count($restoIds) - 1) as $index) {
            $data = [
                'resto_id' => $restoIds[$index],
                'orang_id' => $namaIds[$index],
                'menu_id' => $menuIds[$index],
                'created_at' => now(),
                'updated_at' => now(),
            ];

            // Check if there is an existing record with the same resto_id and menu_id
            $existingRecord = $existingRecords->first(function ($record) use ($data) {
                return $record->resto_id == $data['resto_id'] && $record->menu_id == $data['menu_id'];
            });

            if ($existingRecord) {
                // Update the existing record
                $existingRecord->update($data);
            } else {
                // Insert a new record
                $dataToInsert[] = $data;
            }

            // Track the existing menu_id values
            $existingMenuIds[] = $data['menu_id'];
        }

        // Delete records that are no longer associated with the resto_id
        Tagihan::where('resto_id', $id)
            ->whereNotIn('menu_id', $existingMenuIds)
            ->delete();

        // Use Eloquent's insert method to insert multiple new records in a single query
        Tagihan::insert($dataToInsert);

        $tagihan1 = Tagihan::join('menu', 'tagihan.menu_id', '=', 'menu.id')
            ->join('orang', 'tagihan.orang_id', '=', 'orang.id')
            ->where('orang.resto_id', $id)
            ->select(
                'orang.nama',
                DB::raw('SUM(menu.jumlah_pesanan * menu.harga) as total_harga')
            )
            ->groupBy('orang.nama')
            ->get();

        $menu = Menu::where('resto_id', $id)->get();
        $orang = Orang::where('resto_id', $id)->get();

        $tagihan = Tagihan::join('menu', 'tagihan.menu_id', '=', 'menu.id')
            ->where('menu.resto_id', $id)
            ->select(
                'menu.menu',
                'menu.harga',
                DB::raw('SUM(menu.jumlah_pesanan) as total_pesanan'),
                DB::raw('SUM(menu.jumlah_pesanan * menu.harga) as total_harga')
            )
            ->groupBy('menu.menu', 'menu.harga')
            ->get();
        $tagihan3 = Tagihan::where('resto_id', $id)->get();

        $resto = Resto::where('id', $id)->firstOrFail();

        return view('user.detail_tagihan', compact('resto', 'tagihan1', 'menu', 'orang', 'tagihan', 'tagihan3'));
    }



    public function tagihan_destroy(string $id)
    {
        $menu = Menu::find($id);

        if (!$menu) {
            return response()->json(['error' => 'Menu item not found'], 404);
        }

        // Get the associated Tagihan records
        $tagihan = $menu->tagihan;

        // Delete each Tagihan record
        $tagihan->each(function ($tagihan) {
            $tagihan->delete();
        });

        // Delete the Menu item
        $menu->delete();

        // Return a success response
        return response()->json(['success' => true]);
    }


    public function detail_tagihan($id)
    {
        $tagihan1 = Tagihan::join('menu', 'tagihan.menu_id', '=', 'menu.id')
            ->join('orang', 'tagihan.orang_id', '=', 'orang.id')
            ->where('orang.resto_id', $id) // Filter based on resto_id
            ->select(
                'orang.nama',
                DB::raw('SUM(menu.jumlah_pesanan * menu.harga) as total_harga')
            )
            ->groupBy('orang.nama')
            ->get();

        $menu = Menu::where('resto_id', $id)->get();
        $orang = Orang::where('resto_id', $id)->get();

        $tagihan = Tagihan::join('menu', 'tagihan.menu_id', '=', 'menu.id')
            ->where('menu.resto_id', $id) // Filter based on resto_id
            ->select(
                'menu.menu',
                'menu.harga',
                DB::raw('SUM(menu.jumlah_pesanan) as total_pesanan'),
                DB::raw('SUM(menu.jumlah_pesanan * menu.harga) as total_harga')
            )
            ->groupBy('menu.menu', 'menu.harga')
            ->get();
        $tagihan3 = Tagihan::where('resto_id', $id)->get();

        $resto = Resto::where('id', $id)->firstOrFail();

        return view('user.detail_tagihan', compact('resto', 'tagihan1', 'menu', 'orang', 'tagihan', 'tagihan3'));
    }
}