<?php

namespace App\Mail;

use App\Models\User;
use App\Models\Orang;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Http\Controllers\SendEmail;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Mail\Mailables\Attachment;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Events\Dispatchable;

class SplitBills extends Mailable implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;
    

    /**
     * Create a new message instance.
     *
     * @param $user
     * 
     */
    public function __construct($user)
    {
        $this->user = $user;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('arya@gmail.com', 'Arya Ananda')
            ->subject('Split Bills')
            ->view('mail.autoemail');
    }

    /**
     * Get the message envelope.
     *
     * @return Envelope
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            from: new Address('arya@gmail.com', 'Arya Ananda'),
            subject: 'Split Bills'
        );
    }

    public function attachments()
    {
        return [
            Attachment::fromPath(public_path('pdf/nft.pdf'))
            ->as('nft.pdf')
                ->withMime('application/pdf')
        ];
    }
}