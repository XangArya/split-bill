<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Split Bill</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  </head>
  <body>  
    <table class="table table-hover">
        <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Restoran</th>
      <th scope="col">Jumlah Orang</th>
      <th scope="col">Menu</th>
      <th scope="col">Jumlah Tagihan</th>
      <th scope="col">Gambar Menu</th>
    </tr>
  </thead>
  <tbody>
    @foreach($split as $index => $data)
    <tr>
      <th scope="row">1</th>
      <td>{{ $data['nama_restoran'] }}</td>
      <td>{{ $data['jumlah_orang'] }}</td>
      <td>{{ $data['menu'] }}</td>
      <td>{{ $data['jumlah_tagihan'] }}</td>
      <td>{{ $data['gambar_menu'] }}</td>
    </tr>
    @endforeach
  </tbody>
      </table>  
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  </body>
</html>