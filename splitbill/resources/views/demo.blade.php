<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Split Bill</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> </head>
  <body>  
  <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button>
<p>Nama:</p>
        <ul id="daftarNama"></ul>
        <p>Menu Yang Dipesan:</p>
        <ul id="detailMakananModalTitle"></ul>
        <ul id="daftarMakanan"></ul>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm">
          <div class="input-group mb-3">
            <input type="text" class="form-control" id="namaInput" placeholder="Tambahkan orang">
            <div class="input-group-append">
              <button class="btn btn-success" type="submit" id="tambahButton">+</button>
            </div>
          </div>
        </form>
        <p>Nama:</p>
        <ul id="daftarNama"></ul>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal untuk Detail Makanan -->
<div class="modal fade" id="detailMakananModal" tabindex="-1" role="dialog" aria-labelledby="detailMakananModalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="detailMakananModalTitle">Daftar Makanan yang Dipesan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="makananForm">
          <div class="form-group">
            <label for="makananInput">Tambahkan makanan:</label>
            <input type="text" class="form-control" id="makananInput" placeholder="Makanan">
          </div>
        </form>
        <p>Daftar Makanan:</p>
        <ul id="daftarMakanan"></ul>
        <!-- Tambahkan input harga pada formulir makanan -->
<div class="form-group">
  <label for="makananInput">Tambahkan makanan:</label>
  <input type="text" class="form-control" id="makananInput" placeholder="Makanan">
  <label for="hargaInput">Harga:</label>
  <input type="number" class="form-control" id="hargaInput" placeholder="Harga">
</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="button" class="btn btn-primary" id="simpanMakananButton">Simpan</button>
      </div>
    </div>
  </div>
</div>

<script>
 document.addEventListener('DOMContentLoaded', function() {
  var form = document.getElementById('myForm');
  var namaInput = document.getElementById('namaInput');
  var daftarNama = document.getElementById('daftarNama');
  var makananForm = document.getElementById('makananForm');
  var makananInput = document.getElementById('makananInput');
  var hargaInput = document.getElementById('hargaInput'); // Tambahkan ini
  var daftarMakanan = document.getElementById('daftarMakanan');
  var simpanMakananButton = document.getElementById('simpanMakananButton');
  
  // Objek untuk menyimpan daftar makanan beserta harganya berdasarkan nama
  var makananPerOrang = {};

  form.addEventListener('submit', function(event) {
    event.preventDefault();
    var nama = namaInput.value;
    var listItem = document.createElement('li');
    listItem.textContent = nama;
    
    var detailButton = document.createElement('button');
    detailButton.textContent = 'Detail';
    detailButton.className = 'btn btn-info mr-2';
    
    var editButton = document.createElement('button');
    editButton.textContent = 'Edit';
    editButton.className = 'btn btn-primary mr-2';
    
    var deleteButton = document.createElement('button');
    deleteButton.textContent = 'Hapus';
    deleteButton.className = 'btn btn-danger';
    
    listItem.appendChild(detailButton);
    listItem.appendChild(editButton);
    listItem.appendChild(deleteButton);
    
    detailButton.addEventListener('click', function() {
      // Set nama yang akan ditampilkan di modal detail makanan
      document.getElementById('detailMakananModalTitle').textContent = 'Daftar Makanan yang Dipesan oleh ' + nama;
      
      // Kosongkan daftar makanan yang mungkin ada dari detail sebelumnya
      daftarMakanan.innerHTML = '';
      
      // Mengambil daftar makanan berdasarkan nama
      var makananOrangIni = makananPerOrang[nama] || [];
      
      // Menampilkan daftar makanan yang telah dipesan
      for (var i = 0; i < makananOrangIni.length; i++) {
        var makananItem = document.createElement('li');
        makananItem.textContent = makananOrangIni[i].nama + ' (Harga: ' + makananOrangIni[i].harga + ')';
        daftarMakanan.appendChild(makananItem);
      }
      
      // Simpan nama yang saat ini diproses
      var namaSaatIni = nama;
      
      // Menampilkan modal detail makanan
      $('#detailMakananModal').modal('show');
      
      // Mengatur aksi saat tombol Simpan di modal detail makanan ditekan
      simpanMakananButton.addEventListener('click', function() {
        var makanan = makananInput.value;
        var harga = hargaInput.value; // Ambil nilai harga
        makananOrangIni.push({ nama: makanan, harga: harga }); // Simpan nama dan harga
        var makananItem = document.createElement('li');
        makananItem.textContent = makanan + ' (Harga: ' + harga + ')';
        daftarMakanan.appendChild(makananItem);
        makananInput.value = ''; // Kosongkan input setelah menambahkan makanan
        hargaInput.value = ''; // Kosongkan input harga
        // Simpan daftar makanan yang terkait dengan orang ini
        makananPerOrang[namaSaatIni] = makananOrangIni;
      });
    });
    
    editButton.addEventListener('click', function() {
      var editedNama = prompt('Edit Nama:', nama);
      if (editedNama !== null) {
        // Perbarui daftar makanan jika nama diubah
        var makananOrangIni = makananPerOrang[nama] || [];
        makananPerOrang[editedNama] = makananOrangIni;
        delete makananPerOrang[nama];
        
        listItem.textContent = editedNama;
        listItem.appendChild(detailButton);
        listItem.appendChild(editButton);
        listItem.appendChild(deleteButton);
      }
    });
    
    deleteButton.addEventListener('click', function() {
      // Hapus daftar makanan yang terkait dengan orang ini
      delete makananPerOrang[nama];
      
      listItem.remove();
    });
    
    daftarNama.appendChild(listItem);
    namaInput.value = ''; // Kosongkan input setelah menambahkan nama
    $('#exampleModalCenter').modal('show'); // Menampilkan modal
  });
});

</script>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  </body>
</html>