@extends('user.layouts.app')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="content">
        <div class="row">
          <div class="col-12">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
              <i class="fas fa-plus"></i>Tambah Menu
            </button>
            <!-- Modal -->
            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Restoran</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form action="{{ route('menu.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                      <div class="form-group">
                        <label for="menu">Nama Menu</label>
                        <input type="text" class="form-control" id="menu" name="menu" placeholder="Isi Nama Menu" required />
                      </div>
                      <div class="form-group">
                        <label for="harga">Harga</label>
                        <input type="number" class="form-control" id="harga" name="harga" placeholder="Isi Harga Pesanan" required />
                      </div>
                      <div class="form-group">
                        <label for="pesanan">Jumlah Pesanan</label>
                        <input type="number" class="form-control" id="pesanan" name="pesanan" placeholder="Isi Jumlah Pesanan" required />
                      </div>
                      <input type="hidden" name="jumlah_pesanan" value="1">
                      <input type="hidden" name="resto_id" value="{{ $resto->id }}">
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8 mt-3">
                <h5 class="text-center">Ricin Pesanan</h5>
                <form action="{{ route('tagihan.store', ['id' => $resto]) }}" method="POST">
                  @csrf
                  <table id="example" class="table table-striped" style="width:100%">
                    <thead>
                      <tr>
                        <th>Menu</th>
                        <th>Nama Orang</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      @for ($i = 0; $i < count($menu) || $i < count($tagihan); $i++) @php $menuItem=isset($menu[$i]) ? $menu[$i] : null; $tagihanItem=isset($tagihan[$i]) ? $tagihan[$i] : null; @endphp <tr id="menu_item_{{ $menuItem->id }}">
                        <td>
                          <div class="form-group">
                            @if ($menuItem)
                            <input class="form-control" type="text" placeholder="{{ $menuItem->menu }}" readonly>
                            <input type="hidden" name="menu_id[]" value="{{ $menuItem->id }}">
                            <input type="hidden" name="resto_id[]" value="{{ $resto->id }}">
                            @endif
                          </div>
                        </td>
                        <td>
                          <div class="form-group">
                            <select required class="form-control" name="orang_id[]">
                              <option selected disabled>Nama Orang</option>
                              @foreach($orang as $person)
                              <option value="{{ $person->id }}" {{ $tagihanItem && $person->id == optional($tagihanItem)->orang_id ? 'selected' : '' }}>
                                {{ $person->nama }}
                              </option>
                              @endforeach
                            </select>
                          </div>
                        </td>
                        <td>
                          <div class="form-group ">
                            <button type="button" class="btn btn-danger btn-sm btn-circle" title="Delete event" onclick="deleteMenuItem(this, {{ $menuItem->id }})">
                              <i class="fa-regular fa-trash-can"></i>
                            </button>
                          </div>
                        </td>
                        </tr>
                        @endfor
                    </tbody>
                  </table>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Lihat Tagihan</button>
                  </div>
                </form>
                <a href="/detail_tagihan/{{ $resto->id }}">Lihat Riwayat Tagihan >></a>
                <div class="mt-3">
                <a href="/" class="btn btn-primary">Kembali</a>
                </div>
              </div>
              <div class="col-md-4">
                <form action="{{ route('orang.store') }}" method="POST">
                  @csrf
                  <div class="form-row">
                    <div class="col-md-8">
                      <label for="nama">Tambah Orang</label>
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="Isi Nama Orang" required />
                    </div>
                    <input type="hidden" name="resto_id" value="{{ $resto->id }}">
                    <div class="col-md-4 mt-auto">
                      <button type="submit" class="btn btn-primary">Tambah</button>
                    </div>
                </form>
                <div class="col-12 mt-3">
                  <div class="card">
                    <div class="card-header bg-dark">
                      Daftar Orang
                    </div>
                    <ul class="list-group list-group-flush">
                      @foreach ( $orang as $data)
                      <li class="list-group-item d-flex"><b class="mt-auto mb-auto">{{ $data->nama}}</b>
                        <form action="{{ route('orang.destroy', ['id' => $data->id]) }}" class="ml-auto" style="display:inline" method="POST">
                          @csrf
                          @method('delete')
                          <button type="submit" class="btn btn-outline-danger btn-sm btn-circle" title="Delete event" onclick="return confirm('Yakin?')">
                            <i class="fa-regular fa-trash-can"></i>
                          </button>
                        </form>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
  src = "https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js" >
</script>

<script type="text/javascript">
  // Menambahkan event listener ke dropdown menu
  document.getElementById('menu_id').addEventListener('change', function() {
    // Mengambil harga yang terkait dengan menu yang dipilih
    const selectedOption = this.options[this.selectedIndex];
    const harga = selectedOption.getAttribute('data-harga');

    // Mengisi input harga dengan harga yang dipilih
    document.getElementById('harga').value = harga;
  });
</script>
<script>
  function deleteMenuItem(button, menuItemId) {
    if (confirm('Yakin?')) {
      // Send an AJAX request to delete the menu item from the database
      $.ajax({
        url: `/tagihan/${menuItemId}`,
        type: 'DELETE',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          if (data.success) {
            // Remove the table row from the DOM
            var row = button.closest('tr');
            row.remove();
          }
        },
        error: function() {
          // Handle errors if the deletion fails
          alert('Failed to delete menu item.');
        }
      });
    }
  }
</script>
@endsection
