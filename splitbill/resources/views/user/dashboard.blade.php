@extends('user.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="content">
                <div class="row">
                    <div class="col-md-6">
                        <!-- TO DO List -->
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="ion ion-clipboard mr-1"></i>
                                    List Restoran Sering di Kunjungi Pada Bulan Ini
                                </h3>

                                <div class="card-tools">
                                    <ul class="pagination pagination-sm">
                                        <li class="page-item"><a href="#" class="page-link">&laquo;</a></li>
                                        <li class="page-item"><a href="#" class="page-link">1</a></li>
                                        <li class="page-item"><a href="#" class="page-link">2</a></li>
                                        <li class="page-item"><a href="#" class="page-link">3</a></li>
                                        <li class="page-item"><a href="#" class="page-link">&raquo;</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <ul class="todo-list" data-widget="todo-list">
                                    @php
                                    // Convert $restoPercentages to a collection
                                    $restoCollection = collect($restoPercentages);

                                    // Filter restoran berdasarkan bulan
                                    $filteredRestos = $restoCollection->filter(function ($item) {
                                        // Make sure $item is an object with 'created_at' property
                                        return is_object($item) && property_exists($item, 'created_at') &&
                                            Carbon\Carbon::parse($item->created_at)->month == Carbon\Carbon::now()->month;
                                    });

                                    // Convert the collection back to an array
                                    $filteredRestosArray = $filteredRestos->toArray();

                                    // Now you can use arsort
                                    arsort($filteredRestosArray);
                                    @endphp
                                    @foreach($filteredRestosArray as $namaResto => $percentage)
                                    <li class="list-group-item d-flex justify-content-between align-items-center">
                                        {{ $namaResto }}
                                        <span class="badge badge-primary badge-pill">{{ number_format($percentage, 2) }}%</span>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                    <i class="fas fa-chart-pie mr-1"></i>
                                    Sales
                                </h3>
                            </div><!-- /.card-header -->
                            <div class="card-body">
                                <canvas id="myChart"></canvas>
                            </div><!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const ctx = document.getElementById('myChart');
    function getRandomRGB(count) {
    const random255 = () => Math.floor(Math.random() * 256);
    return Array.from({ length: count }, () => `rgb(${random255()}, ${random255()}, ${random255()})`);
}

const numberOfColors = Object.keys(@json($filteredRestos)).length;

new Chart(ctx, {
    type: 'doughnut',
    data: {
        labels: [
            @foreach($filteredRestos as $namaResto => $percentage)
                '{{ $namaResto }}',
            @endforeach
        ],
        datasets: [{
            label: 'My First Dataset',
            data: [
                @foreach($filteredRestos as $namaResto => $percentage)
                    {{ $percentage }},
                @endforeach
            ],
            backgroundColor: getRandomRGB(numberOfColors),
            hoverOffset: 4
        }]
    }
});
</script>
@endsection
