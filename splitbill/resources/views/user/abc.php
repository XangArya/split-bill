<h5 class="text-center">Ricin Pesanan</h5>
<table id="example" class="table table-striped" style="width:100%">
    <thead>
        <tr>
            <th>Menu</th>
            <th>Jumlah</th>
            <th>Total</th>
            <th>Subtotal</th>
            <th width="100px">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ( $tagihan as $data)
        <tr>
            <td>{{ $data->menu }}</td>
            <td>{{ $data->total_pesanan }}</td>
            <td>{{ $data->harga }}</td>
            <td>{{ $data->total_harga }}</td>
            <td>
                <form action="/menu/{{ $resto->id }}" method="GET">
                    <button type="submit" class="btn btn-success btn-sm btn-circle">
                        Detail Pesanan
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>