@extends('user.layouts.app')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="content">
                <div class="row">
                    <div class="col-12">
                        <div class="col-md-8 mx-auto">
                            <div class="card card-primary">
                                <div class="card-header">
                                    <h5 class="text-center">Ricin Pesanan</h5>
                                </div>
                                <div class="card-body">
                                    <table id="example" class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Menu</th>
                                                <th>Jumlah</th>
                                                <th>Total</th>
                                                <th>Subtotal</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ( $tagihan as $data)
                                            <tr>
                                                <td>{{ $data->menu }}</td>
                                                <td>{{ $data->total_pesanan }}</td>
                                                <td>
                                                    @if(isset($data->harga))
                                                    {{ 'Rp ' . number_format($data->harga, 0, ',', '.') }}
                                                    @else
                                                    N/A
                                                    @endif
                                                </td>
                                                <td>{{ 'Rp ' . number_format($data->total_harga, 0, ',', '.') }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="col-md-8 mx-auto">
                            <div class="card card-primary mt-5">
                                <div class="card-header">
                                    <h5 class="text-center">Table Menu Berdasarkan Orang</h5>
                                </div>
                                <div class="card-body">
                                    <table id="example2" class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Menu</th>
                                                <th>Orang</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ( $tagihan3 as $data)
                                            <tr>
                                                <td>{{ $data->menu->menu }}</td>
                                                <td>{{ $data->orang->nama }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="col-md-8 mx-auto">
                            <div class="card card-primary mt-5">
                                <div class="card-header">
                                    <h5 class="text-center">Tabel Tagihan Per-Orang</h5>
                                </div>
                                <div class="card-body">
                                    <table id="example1" class="table table-striped" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th>Orang</th>
                                                <th>Bayar</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ( $tagihan1 as $data)
                                            <tr>
                                                <td>{{ $data->nama }}</td>
                                                <td>{{ 'Rp ' . number_format($data->total_harga, 0, ',', '.') }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <a href="/tagihan/{{ $resto->id }}" class="btn btn-primary">Kembali</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#example').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        });
    });
    $(document).ready(function() {
        $('#example1').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        });
    });
    $(document).ready(function() {
        $('#example2').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]
        });
    });
</script>
@endsection