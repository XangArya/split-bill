@extends('user.layouts.app')
@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="content">
        <div class="row">
          <div class="col-12">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addModal">
              <i class="fas fa-plus"></i>Tambah Restoran
            </button>

            <!-- Modal -->
            <div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
              <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Tambah Restoran</h5>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <form id="addMenu" action="{{ route('resto.store') }}" method="POST">
                    @csrf
                    <div class="modal-body">
                      <div class="form-group">
                        <label for="nama_resto">Nama Restoran</label>
                        <input type="text" class="form-control" id="nama_resto" name="nama_resto" placeholder="Isi Berita Terkini" required />
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                      <button type="submit" class="btn btn-primary">Tambahkan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 mt-3" id="menu-list">

          </div>

        </div>
      </div>
    </div>
  </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
$(document).ready(function() {
  // Function to create restaurant details HTML
  function createRestoDetails(response) {
  return `
    <div class="card menu-list">
      <h5 class="card-header d-flex">${response.nama_resto}
        <button type="button" class="btn btn-primary btn-sm btn-circle ml-auto" data-bs-toggle="modal" data-bs-target="#editModal/${response.id}">
          <i class="fas fa-edit"></i>
        </button>                 
        <div class="modal fade" id="editModal/${response.id}" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="editModalTitle">Edit Restoran</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <form id="editMenu" action="/resto/${response.id}" method="POST">
                @csrf
                <div class="modal-body">
                  <div class="form-group">
                    <label for="nama_resto">Nama Restoran</label>
                    <input type="text" class="form-control" id="nama_resto" name="nama_resto" value="${response.nama_resto}" placeholder="Isi Berita Terkini" required />
                  </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                  <button type="submit" class="btn btn-primary">Tambahkan</button>
                </div>
              </form>
            </div>
          </div>
        </div>
        <form action="/resto/${response.id}" class="ml-2" style="display:inline" method="POST">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="_method" value="DELETE">
          <button type="submit" class="btn btn-danger btn-sm btn-circle" title="Delete event" onclick="return confirm('Yakin?')">
            <i class="fas fa-trash"></i>
          </button>
        </form>
      </h5>
      <div class="card-body">
        <h5 class="card-title"></h5>
        <p class="card-text">Silahkan membagi biaya tagihan.</p>
        <a href="/tagihan/${response.id}" class="btn btn-primary">Lanjut</a>
      </div>
    </div>
  `;
}
  // Function to handle form submission (add/edit)
  function handleFormSubmission(form, successCallback) {
    var formData = new FormData(form[0]);

    $.ajax({
      url: form.attr('action'),
      method: 'POST',
      data: formData,
      dataType: 'JSON',
      contentType: false,
      cache: false,
      processData: false,
      success: successCallback,
      error: function(error) {
        console.error('Error:', error);
      }
    });
  }

  // Submitting the add menu form
  $('#addMenu').on('submit', function(event) {
    event.preventDefault();
    var form = $(this);

    handleFormSubmission(form, function(response) {
      var restoDetails = createRestoDetails(response);
      $('#menu-list').prepend(restoDetails);
      form[0].reset();
      $('#addModal').modal('hide');
    });
  });

  // Submitting the edit menu form
  $('#menu-list').on('submit', '#editMenu', function(event) {
  event.preventDefault();
  var form = $(this);

  handleFormSubmission(form, function(response) {
    var editedRestoDetails = createRestoDetails(response);
    $('#menu-list').prepend(editedRestoDetails);  // Use prepend instead of html
    // Close the corresponding edit modal after a successful update
    var modalId = `#editModal/${response.id}`;
    $(modalId).modal('hide');
  });
});
});
</script>

@endsection