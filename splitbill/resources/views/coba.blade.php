<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Split Bill</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"> </head>
  <body>  
  <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
  Launch demo modal
</button>
<p>Nama:</p>
        <ul id="daftarNama"></ul>
<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="myForm">
          <div class="input-group mb-3">
            <input type="text" class="form-control" id="namaInput" placeholder="Tambahkan orang">
            <div class="input-group-append">
              <button class="btn btn-success" type="submit" id="tambahButton">+</button>
            </div>
          </div>
        </form>
        <p>Nama:</p>
        <ul id="daftarNama"></ul>
      </div>
     
    </div>
  </div>
</div>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var form = document.getElementById('myForm');
    var namaInput = document.getElementById('namaInput');
    var daftarNama = document.getElementById('daftarNama');

    form.addEventListener('submit', function(event) {
      event.preventDefault();
      var nama = namaInput.value;
      var listItem = document.createElement('li');
      listItem.textContent = nama;
      daftarNama.appendChild(listItem);
      namaInput.value = ""; // Kosongkan input setelah menambahkan nama
      $('#exampleModalCenter').modal('show'); // Menampilkan modal
    });
  });
</script>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  </body>
</html>