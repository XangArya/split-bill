<?php

use App\Http\Controllers\SendEmail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MenuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', 'App\Http\Controllers\MenuController@dashboard');
Route::get('/splitbill', 'App\Http\Controllers\MenuController@resto');
Route::post('/splitbill', 'App\Http\Controllers\MenuController@resto_store')->name('resto.store');
Route::delete('/resto/{id}', 'App\Http\Controllers\MenuController@resto_destroy')->name('resto.destroy');
Route::post('/resto/{id}', 'App\Http\Controllers\MenuController@resto_update')->name('resto.update');
Route::get('/tagihan/{id}', 'App\Http\Controllers\MenuController@tagihan');
Route::post('/orang', 'App\Http\Controllers\MenuController@orang_store')->name('orang.store');
Route::delete('/orang/{id}', 'App\Http\Controllers\MenuController@orang_destroy')->name('orang.destroy');
Route::post('/menu', 'App\Http\Controllers\MenuController@menu_store')->name('menu.store');
Route::put('/menu/{id}', 'App\Http\Controllers\MenuController@menu_update')->name('menu.update');
Route::delete('/menu/{id}', 'App\Http\Controllers\MenuController@menu_destroy')->name('menu.destroy');
Route::post('/detail_tagihan/{id}', 'App\Http\Controllers\MenuController@tagihan_store')->name('tagihan.store');
Route::patch('detail_tagihan/{id}', 'App\Http\Controllers\MenuController@tagihan_store')->name('tagihan.store');
Route::delete('/tagihan/{id}', 'App\Http\Controllers\MenuController@tagihan_destroy')->name('tagihan.destroy');
Route::get('/detail_tagihan/{id}', 'App\Http\Controllers\MenuController@detail_tagihan');
Route::get('/menu/{id}', 'App\Http\Controllers\MenuController@detail_pesanan')->name('menu');
Route::get('send-email', [SendEmail::class, 'index']);